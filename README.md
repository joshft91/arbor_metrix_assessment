Running the XmlToJson class will take in the following example XML file from the `main/resources` folder.

    <?xml version="1.0" encoding="UTF-8"?>
    <patients>
      <patient>
          <id>1234</id>
          <gender>m</gender>
          <name>John Smith</name>
          <state>Michigan</state>
          <dateOfBirth>03/04/1962</dateOfBirth>
      </patient>
      <patient>
          <id>5678</id>
          <gender>f</gender>
          <name>Jane Smith</name>
          <state>Ohio</state>
          <dateOfBirth>08/24/1971</dateOfBirth>
      </patient>
    </patients>
  
  The program will output the following JSON in accordance the manipulation file specified in `main/resources/xml_translations.json`
  
      {
        "": [
          {
            "name": "John Smith",
            "patientid": 1234,
            "sex": "male",
            "state": "MI",
            "age": 58
          },
          {
            "name": "Jane Smith",
            "patientid": 5678,
            "sex": "female",
            "state": "OH",
            "age": 49
          }
        ]
      }
      
  Unfortunately it's not sophisticated enough to handle any XML structure and instead really only works for this specific example.
  Future improvements would include:
  - Different XML parsing library to make it easier to manipulate the XML tree
  - Better translation json to better describe how to change both field names and values