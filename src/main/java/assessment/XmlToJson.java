package assessment;

import com.google.gson.*;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class XmlToJson {

    private static final String XML_TRANSLATIONS_FILE = "xml_translations.json";
    private static final String PATIENTS_TEST_XML = "patients_test.xml";

    public static void main(String[] args) {
        XmlToJson xmlToJson = new XmlToJson();

        TranslationFile translationFile = xmlToJson.getTranslationFile();
        Element xmlElement = xmlToJson.getXmlDocument(PATIENTS_TEST_XML);

        String translatedJson = xmlToJson.performTranslation(translationFile, xmlElement);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonElement jsonElement = JsonParser.parseString(translatedJson);

        System.out.println(gson.toJson(jsonElement));
    }


    /**
     * Takes in a TranslationFile and XML Element and translates the XML
     * to JSON as defined in the translation file.
     *
     * @return returns JSON in a string format of the translated fields
     */
    private String performTranslation(TranslationFile translationFile, Element xml) {
        List<TranslationFile.FieldList> fieldLists = translationFile.getFieldLists();

        // Start new translated JSON object
        JsonObject translatedJson = new JsonObject();

        // Iterate through each of the field lists defined in the translation file
        for (TranslationFile.FieldList fieldList : fieldLists) {
            translatedJson.add("", translateXmlListToJson(fieldList, xml));
        }

        return translatedJson.toString();
    }

    /**
     * @param fieldList the FieldList information to perform translations
     * @param element   the root XML Element
     * @return a
     */
    private JsonArray translateXmlListToJson(TranslationFile.FieldList fieldList, Element element) {
        // Since each FieldList represents a JsonArray, create that now
        JsonArray array = new JsonArray();

        // Get the sub field for each XML list to search for field translations
        String listSubField = fieldList.getListSubField();

        // Get the field translations to apply to each element when necessary
        List<TranslationFile.FieldTranslation> fieldTranslations = fieldList.getFieldTranslations();

        // Get the list of nodes to look for fields to change (<patient>)
        NodeList childNodeList = element.getElementsByTagName(listSubField);
        int childNodeListLength = childNodeList.getLength();

        // Iterate through each of the XML and get the child nodes for each one
        for (int i = 0; i < childNodeListLength; i++) {
            // Cast each child node to an element to make lookup of the field translation easier
            Element childElement = (Element) childNodeList.item(i);

            // For each field translation, perform the translation and add to the new JsonObject
            JsonObject newObject = new JsonObject();
            for (TranslationFile.FieldTranslation fieldTranslation : fieldTranslations) {

                // Check if there's an element (should only be one) to perform a translation on
                NodeList nodeToTranslate = childElement.getElementsByTagName(fieldTranslation.getOldFieldName());
                if (nodeToTranslate.getLength() != 0) {
                    TranslationUtils.performTranslation((Element) nodeToTranslate.item(0), fieldTranslation, newObject);
                }
            }
            array.add(newObject);
        }

        return array;
    }

    /**
     * Gets the {@link TranslationFile} to determine mapping logic to JSON
     */
    private TranslationFile getTranslationFile() {
        InputStreamReader translationFileReader = new InputStreamReader(getResourceFileAsInputStream(XML_TRANSLATIONS_FILE));

        return new Gson().fromJson(translationFileReader, TranslationFile.class);
    }

    /**
     * Loads the XML file and returns the root element
     */
    private Element getXmlDocument(String resourceFile) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            return builder.parse(getResourceFileAsInputStream(resourceFile)).getDocumentElement();
        } catch (Exception e) {
            // Handle exception; for this assessment, assume XML file is always available and parses correctly
            throw new RuntimeException("Something went wrong");
        }
    }

    private InputStream getResourceFileAsInputStream(String file) {
        return XmlToJson.class.getResourceAsStream("/" + file);
    }
}
