package assessment;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Represents the translation file as an object
 * to determine how to map XML elements into JSON.
 */
public class TranslationFile {

    @SerializedName("LISTS")
    public List<FieldList> fieldLists;

    public List<FieldList> getFieldLists() {
        return fieldLists;
    }

    public class FieldList {
        public String listField;
        public String listSubField;

        @SerializedName("FIELD_TRANSLATIONS")
        public List<FieldTranslation> fieldTranslations;

        public String getListField() {
            return listField;
        }

        public String getListSubField() {
            return listSubField;
        }

        public List<FieldTranslation> getFieldTranslations() {
            return fieldTranslations;
        }
    }

    public class FieldTranslation {
        public String oldFieldName;
        public String newFieldName;
        public String logic;

        public String getOldFieldName() {
            return oldFieldName;
        }

        public String getNewFieldName() {
            return newFieldName;
        }

        public String getLogic() {
            return logic;
        }
    }
}
