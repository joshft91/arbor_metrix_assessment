package assessment;

import com.google.gson.JsonObject;
import org.w3c.dom.Element;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class TranslationUtils {

    private static final String STRING_TO_NUMBER_LOGIC = "STRING_TO_NUMBER";
    private static final String GENDER_TO_SEX_LOGIC = "GENDER_TO_SEX";
    private static final String STATE_ABBREVIATION_LOGIC = "STATE_ABBREVIATION";
    private static final String CONVERT_TO_AGE_LOGIC = "CONVERT_TO_AGE";

    /**
     * Performs the translation on the fields and adds the properties to the supplied JsonObject
     */
    public static void performTranslation(Element element, TranslationFile.FieldTranslation fieldTranslation, JsonObject jsonObject) {
        String newPropertyName = fieldTranslation.getNewFieldName();
        String fieldTextValue = element.getTextContent();

        String logic = fieldTranslation.getLogic();
        if (logic != null) {
            switch (logic) {
                case STRING_TO_NUMBER_LOGIC:
                    jsonObject.addProperty(newPropertyName, convertStringToNumber(fieldTextValue));
                    break;
                case GENDER_TO_SEX_LOGIC:
                    jsonObject.addProperty(newPropertyName, convertGenderToSex(fieldTextValue));
                    break;
                case STATE_ABBREVIATION_LOGIC:
                    jsonObject.addProperty(newPropertyName, getStateAbbreviation(fieldTextValue));
                    break;
                case CONVERT_TO_AGE_LOGIC:
                    jsonObject.addProperty(newPropertyName, convertDateToAge(fieldTextValue));
                    break;
                default:
                    break; // Do nothing
            }
        } else {
            jsonObject.addProperty(newPropertyName, fieldTextValue);
        }
    }


    private static int convertStringToNumber(String string) {
        return Integer.parseInt(string);
    }

    private static String convertGenderToSex(String gender) {
        if (gender.equalsIgnoreCase("m")) {
            return "male";
        } else {
            return "female";
        }
    }

    private static String getStateAbbreviation(String stateName) {
        return State.valueOfName(stateName).getAbbreviation();
    }

    private static int convertDateToAge(String date) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate birthDate = LocalDate.parse(date, dateTimeFormatter);
        LocalDate now = LocalDate.now();

        return Period.between(birthDate, now).getYears();
    }
}
